﻿using Commons.Model;
using Microsoft.AspNet.SignalR.Client;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace BeaconsWpf.Services
{
    public delegate void BeaconsReceived(List<BeaconSignals> signals);


    public class BeaconHubService
    {
        public event BeaconsReceived OnBeaconsReceived;

        private readonly HubConnection _connection;

        private readonly IHubProxy _hub;


        public BeaconHubService(string url)
        {
            _connection = new HubConnection(url);
            _connection.Closed += async () => await _connection.Start();

            _hub = _connection.CreateHubProxy("BeaconHub");
            _hub.On<BeaconHubMessage>("Receive", data =>
            {
                OnBeaconsReceived?.Invoke(data.Beacons);
            });
        }

        public async Task Connect()
        {
            await _connection.Start();
        }
    }
}
