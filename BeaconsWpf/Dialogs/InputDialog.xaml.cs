﻿using System.Windows;

namespace BeaconsWpf.Dialogs
{
    /// <summary>
    /// Interaction logic for InputDialog.xaml
    /// </summary>
    public partial class InputDialog : Window
    {
        public InputDialog()
        {
            InitializeComponent();
        }

        public string Text
        {
            get => LabelText.Content as string;

            set => LabelText.Content = value;
        }

        public string ResponseText
        {
            get => ResponseTextBox.Text;

            set => ResponseTextBox.Text = value;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
