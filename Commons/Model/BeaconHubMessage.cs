﻿using System.Collections.Generic;

namespace Commons.Model
{
    public class BeaconHubMessage
    {
        public List<BeaconSignals> Beacons { get; set; }
    }
}
