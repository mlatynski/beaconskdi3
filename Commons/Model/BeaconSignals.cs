﻿using System.Collections.Generic;

namespace Commons.Model
{
    public class BeaconSignals
    {
        public string Mac { get; set; }

        public List<EndpointSignal> EndpointSignals { get; set; }
    }
}
