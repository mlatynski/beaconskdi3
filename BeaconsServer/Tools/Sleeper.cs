﻿using System;
using System.Diagnostics;
using System.Threading;

namespace BeaconsServer.Tools
{
    public class Sleeper
    {
        private Stopwatch _stopwatch;

        private int _time;

        private int _elapsedTime;

        private int _remainTime;


        public Sleeper(TimeSpan time)
        {
            _time = (int)time.TotalMilliseconds;
        }

        public void Init()
        {
            _stopwatch = Stopwatch.StartNew();
        }

        public void Sleep()
        {
            _stopwatch.Stop();
            _elapsedTime = (int)_stopwatch.ElapsedMilliseconds;
            _remainTime = _time - _elapsedTime;

            //Console.WriteLine($"{_elapsedTime} ms");

            if (_remainTime > 0)
            {
                Thread.Sleep(_remainTime);
            }
        }
    }
}
