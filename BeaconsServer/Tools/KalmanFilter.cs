﻿namespace BeaconsServer.Tools
{
    public class KalmanFilter
    {
        private double _a;

        private double _h;

        private double _q;

        private double _r;

        private double _p;

        private double _x;


        public KalmanFilter(double x)
            : this(1, 1, 0.125, 1, 0.1, x)
        {

        }

        public KalmanFilter(double a, double h, double q, double r, double p, double x)
        {
            _a = a;
            _h = h;
            _q = q;
            _r = r;
            _p = p;
            _x = x;
        }

        public void Add(double value)
        {
            // time update - prediction
            _x = _a * _x;
            _p = _a * _p * _a + _q;

            // measurement update - correction
            double k = _p * _h / (_h * _p * _h + _r);
            _x = _x + k * (value - _h * _x);
            _p = (1 - k * _h) * _p;
        }

        public double GetResult()
        {
            return _x;
        }
    }
}
