﻿using BeaconsServer.Services;
using System;

namespace BeaconsServer
{
    static class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Hub url: ");
            string url = Console.ReadLine();
            url = string.IsNullOrEmpty(url) ? "localhost" : url;

            var ioc = new IoC(url);

            Console.WriteLine("Start...");

            var processor = ioc.GetService<IBeaconServer>();
            processor.Run();
        }
    }
}
