﻿using Commons.Model;
using System.Collections.Generic;
using System.Linq;

namespace BeaconsServer.Model
{
    public class BeaconEndpointMap
    {
        private readonly IDictionary<string, EndpointFilterMap> _dictonary;


        public BeaconEndpointMap()
        {
            _dictonary = new Dictionary<string, EndpointFilterMap>();
        }

        public EndpointFilterMap AddAndGet(string mac)
        {
            EndpointFilterMap item = null;

            if (!_dictonary.ContainsKey(mac))
            {
                item = new EndpointFilterMap();
                _dictonary.Add(mac, item);
            }
            else
            {
                item = _dictonary[mac];
            }

            return item;
        }

        public void Clean()
        {
            var emptyKeys = _dictonary.Where(x => x.Value.Count == 0).Select(x => x.Key).ToList();

            foreach (var key in emptyKeys)
            {
                _dictonary.Remove(key);
            }
        }

        public List<BeaconSignals> GetAll()
        {
            return _dictonary.Select(x => new BeaconSignals
            {
                Mac = x.Key,
                EndpointSignals = x.Value.GetAll()
            })
            .ToList();
        }
    }
}
