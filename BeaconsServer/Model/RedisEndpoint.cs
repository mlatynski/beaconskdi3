﻿namespace BeaconsServer.Model
{
    public class RedisEndpoint
    {
        public string Ip { get; set; }

        public RedisDevice Device { get; set; }
    }
}
