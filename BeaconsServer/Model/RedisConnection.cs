﻿using StackExchange.Redis;

namespace BeaconsServer.Model
{
    public class RedisConnection
    {
        public ConnectionMultiplexer Connection { get; set; }

        public RedisEndpoint Endpoint { get; set; }
    }
}
