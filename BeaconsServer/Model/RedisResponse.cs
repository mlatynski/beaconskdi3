﻿using System;

namespace BeaconsServer.Model
{
    public class RedisResponse
    {
        public string Mac { get; set; }

        public double Rssi { get; set; }

        public DateTime Date { get; set; }

        public int EndpointId { get; set; }
    }
}
