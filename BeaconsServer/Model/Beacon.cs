﻿namespace BeaconsServer.Model
{
    public class Beacon
    {
        public string Name { get; set; }

        public string Mac { get; set; }

        public double Tx { get; set; }
    }
}
