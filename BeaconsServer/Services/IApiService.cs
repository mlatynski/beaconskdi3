﻿using BeaconsServer.Model;
using System.Collections.Generic;

namespace BeaconsServer.Services
{
    public interface IApiService
    {
        List<RedisDevice> GetRedisDevices();

        List<Beacon> GetBeacons();
    }
}
