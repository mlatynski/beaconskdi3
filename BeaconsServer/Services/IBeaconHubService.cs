﻿using Commons.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeaconsServer.Services
{
    public interface IBeaconHubService
    {
        Task Connect();

        Task Send(List<BeaconSignals> beacons);
    }
}
