﻿using BeaconsServer.Model;
using System.Collections.Generic;

namespace BeaconsServer.Services
{
    public class ApiService : IApiService
    {
        public List<RedisDevice> GetRedisDevices()
        {
            return new List<RedisDevice>
            {
                new RedisDevice
                {
                    Id = 1,
                    Mac = "B8:27:EB:D2:B4:46",
                    Port = 6379,
                    Database = 0,
                    Key = "raw:rpi1",
                    PositionX = 14.0,
                    PositionY = 2.5
                },

                new RedisDevice
                {
                    Id = 2,
                    Mac = "B8:27:EB:F6:2D:0C",
                    Port = 6379,
                    Database = 0,
                    Key = "raw:rpi2",
                    PositionX = 5.0,
                    PositionY = 7.0
                },

                new RedisDevice
                {
                    Id = 3,
                    Mac = "B8:27:EB:4D:70:35",
                    Port = 6379,
                    Database = 0,
                    Key = "raw:rpi3",
                    PositionX = 6.0,
                    PositionY = 0.0
                }
            };
        }

        public List<Beacon> GetBeacons()
        {
            return new List<Beacon>
            {
                new Beacon
                {
                    Name = "A",
                    Mac = "6C:61:D3:9C:9C:2C",
                    Tx = -59
                },

                new Beacon
                {
                    Name = "B",
                    Mac = "6C:61:D3:9F:A2:2C",
                    Tx = -61
                },

                new Beacon
                {
                    Name = "C",
                    Mac = "6C:61:D3:A4:4D:2B",
                    Tx = -59
                },

                new Beacon
                {
                    Name = "D",
                    Mac = "6C:61:D3:A7:31:2A",
                    Tx = -59
                }
            };
        }
    }
}