﻿namespace BeaconsServer.Services
{
    public interface IBeaconServer
    {
        void Run();
    }
}
