﻿using BeaconsServer.Model;
using Commons.Model;
using MathNet.Numerics.Statistics;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BeaconsServer.Services
{
    public class CollectorService : ICollectorService
    {
        private readonly BeaconEndpointMap _beaconEndpointMap;


        public CollectorService()
        {
            _beaconEndpointMap = new BeaconEndpointMap();
        }

        public List<BeaconSignals> Add(IEnumerable<RedisResponse> data, TimeSpan expirationTime)
        {
            var beacons = Group(data);

            foreach (var beacon in beacons)
            {
                var endpointFilterMap = _beaconEndpointMap.AddAndGet(beacon.Mac);

                foreach (var endpoint in beacon.EndpointSignals)
                {
                    double rssi = endpoint.Signals.First().Rssi;
                    var firstDate = endpoint.Signals.First().Date;
                    var lastDate = endpoint.Signals.Last().Date;

                    endpointFilterMap.Clean(endpoint.Id, firstDate - expirationTime);
                    var filterTimestamp = endpointFilterMap.AddAndGet(endpoint.Id, rssi, lastDate);

                    for (int i = 1; i < endpoint.Signals.Count; i++)
                    {
                        filterTimestamp.Filter.Add(endpoint.Signals[i].Rssi);
                    }
                }

                _beaconEndpointMap.Clean();
            }

            return _beaconEndpointMap.GetAll();
        }

        private IEnumerable<BeaconTimestampSignals> Group(IEnumerable<RedisResponse> data)
        {
            return data
                .Distinct()
                .GroupBy(x => x.Mac)
                .Select(x => new BeaconTimestampSignals
                {
                    Mac = x.Key,
                    EndpointSignals = x.GroupBy(e => e.EndpointId).Select(e => new EndpointTimestampSignals
                    {
                        Id = e.Key,
                        Signals = e.GroupBy(s => s.Date).Select(s => new RssiTimestamp
                        {
                            Date = s.Key,
                            Rssi = s.Select(r => r.Rssi).Median()
                        })
                        .OrderBy(s => s.Date)
                        .ToList()
                    })
                     .ToList()
                })
                .ToList();
        }
    }
}
