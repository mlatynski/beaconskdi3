﻿using Commons.Model;
using Microsoft.AspNet.SignalR.Client;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeaconsServer.Services
{
    public class BeaconHubService : IBeaconHubService
    {
        private readonly HubConnection _connection;

        private readonly IHubProxy _hub;


        public BeaconHubService(string url)
        {
            _connection = new HubConnection(url);
            _connection.Closed += async () => await _connection.Start();

            _hub = _connection.CreateHubProxy("BeaconHub");
        }

        public async Task Connect()
        {
            await _connection.Start();
        }

        public async Task Send(List<BeaconSignals> beacons)
        {
            await _hub.Invoke("Send", new BeaconHubMessage
            {
                Beacons = beacons
            });
        }
    }
}
