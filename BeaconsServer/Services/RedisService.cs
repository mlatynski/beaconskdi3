﻿using BeaconsServer.Model;
using BeaconsServer.Tools;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace BeaconsServer.Services
{
    public class RedisService : IRedisService
    {
        private List<RedisConnection> _connections;


        public RedisService()
        {
            _connections = new List<RedisConnection>();
        }

        public void Connect(IEnumerable<RedisEndpoint> endpoints)
        {
            foreach (var endpoint in endpoints)
            {
                ConnectionMultiplexer connection = null;

                try
                {
                    connection = ConnectionMultiplexer.Connect($"{endpoint.Ip}:{endpoint.Device.Port}");
                    Console.WriteLine($"Connected to {endpoint.Ip}!");
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Can't connect to {endpoint.Ip}. Details: {e.Message}");
                }

                _connections.Add(new RedisConnection
                {
                    Connection = connection,
                    Endpoint = endpoint
                });
            }
        }

        public IEnumerable<RedisResponse> GetData(IEnumerable<string> macWhitelist)
        {
            if (macWhitelist != null)
            {
                macWhitelist = macWhitelist.Select(x => x.ToUpper()).ToList();
            }

            var result = new List<RedisResponse>();

            foreach (var connection in _connections)
            {
                if (connection.Connection == null)
                {
                    continue;
                }

                IDatabase database = connection.Connection.GetDatabase(connection.Endpoint.Device.Database);
                var values = database.ListRange(connection.Endpoint.Device.Key, 0, -1);

                if (values.Length > 0)
                {
                    foreach (var value in values)
                    {
                        var splittedValue = value.ToString().Split(',');

                        string mac = splittedValue[0].ToUpper();

                        if ((macWhitelist == null)  || (macWhitelist != null && macWhitelist.Contains(mac)))
                        {
                            double rssi = double.Parse(splittedValue[1]);

                            double ticks = double.Parse(splittedValue[2], CultureInfo.InvariantCulture);
                            DateTime date = DateConverter.TrimToSeconds(DateConverter.FromUnix(ticks));

                            result.Add(new RedisResponse
                            {
                                EndpointId = connection.Endpoint.Device.Id,
                                Mac = mac,
                                Rssi = rssi,
                                Date = date
                            });
                        }
                    }

                    database.ListTrim(connection.Endpoint.Device.Key, values.Length, -1);
                }
            }

            return result;
        }
    }
}
