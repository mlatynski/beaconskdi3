﻿using BeaconsServer.Model;
using System.Collections.Generic;

namespace BeaconsServer.Services
{
    public class EndpointsScannerService : IEndpointsScannerService
    {
        public List<RedisEndpoint> Get(List<RedisDevice> devices)
        {
            var addresses = Scan();
            var result = new List<RedisEndpoint>();

            foreach (var device in devices)
            {
                if (addresses.ContainsKey(device.Mac))
                {
                    result.Add(new RedisEndpoint
                    {
                        Ip = addresses[device.Mac],
                        Device = device
                    });
                }
            }

            return result;
        }

        private Dictionary<string, string> Scan()
        {
            return new Dictionary<string, string>
            {
                { "B8:27:EB:D2:B4:46", "172.18.20.54" },
                { "B8:27:EB:F6:2D:0C", "172.18.20.57" },
                { "B8:27:EB:4D:70:35", "172.18.20.58" }
            };
        }
    }
}
