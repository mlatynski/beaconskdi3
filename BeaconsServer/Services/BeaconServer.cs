﻿using BeaconsServer.Tools;
using System;
using System.Linq;

namespace BeaconsServer.Services
{
    public class BeaconServer : IBeaconServer
    {
        private readonly IApiService _apiService;

        private readonly IEndpointsScannerService _endpointsScannerService;

        private readonly IRedisService _redisService;

        private readonly ICollectorService _collectorService;

        private readonly IBeaconHubService _beaconHubService;


        public BeaconServer(
            IApiService apiService,
            IEndpointsScannerService endpointsScannerService,
            IRedisService redisService,
            ICollectorService collectorService,
            IBeaconHubService beaconHubService)
        {
            _apiService = apiService;
            _endpointsScannerService = endpointsScannerService;
            _redisService = redisService;
            _collectorService = collectorService;
            _beaconHubService = beaconHubService;
        }

        public void Run()
        {
            var devices = _apiService.GetRedisDevices();
            var endpoints = _endpointsScannerService.Get(devices);
            _redisService.Connect(endpoints);

            //var beacons = _apiService.GetBeacons();
            //var beaconMacs = beacons.Select(x => x.Mac).ToList();

            _beaconHubService.Connect();

            var signalExpirationTime = TimeSpan.FromSeconds(2);
            var sleeper = new Sleeper(TimeSpan.FromSeconds(1));

            while (true)
            {
                sleeper.Init();

                var beaconResponses = _redisService.GetData(null);
                var beaconSignals = _collectorService.Add(beaconResponses, signalExpirationTime);

                _beaconHubService.Send(beaconSignals);

                sleeper.Sleep();
            }
        }
    }
}
