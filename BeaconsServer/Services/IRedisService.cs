﻿using BeaconsServer.Model;
using System.Collections.Generic;

namespace BeaconsServer.Services
{
    public interface IRedisService
    {
        void Connect(IEnumerable<RedisEndpoint> endpoints);

        IEnumerable<RedisResponse> GetData(IEnumerable<string> macWhitelist);
    }
}
